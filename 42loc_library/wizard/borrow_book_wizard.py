# -*- coding: utf-8 -*-

from odoo import models, fields, api
from operator import itemgetter


class BorrowBookWizard(models.TransientModel):
    _name = "library.borrow.book.wizard"
    _description = "Borrow Book"

    date_start = fields.Date(string="Book issued date", required=True)
    date_end = fields.Date(string="Expected return date", required=True)

    customer_id = fields.Many2one("res.partner", string="Customer", required=True)

    def do_action(self):
        stage_id = self.env["library.book.stage"].search([('name', '=', 'On Hand')], limit=1).id
        user = self.env["res.users"].search([('id', '=', self._context.get('uid'))])
        book = self.env["library.book"].search([('id', '=', self._context.get('active_id'))])
        book.write({
            'quantity': book.quantity - 1
        })
        borrow_book = book.copy()
        borrow_book.write({
            'book_stage_id': stage_id,
            'customer_id': self.customer_id
        })
        self.env["calendar.event"].sudo().create({
            'name': borrow_book.name,
            'allday': True,
            'start_date': self.date_start,
            'stop_date': self.date_end,
            'partner_ids': [(6, 0, [user.partner_id.id])],
            'user_id': self._context.get('uid'),
            'book_id': borrow_book.id
        })
        return borrow_book
