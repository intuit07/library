# -*- coding: utf-8 -*-
from odoo import http
from odoo.addons.web.controllers.main import ensure_db
from odoo.addons.website.controllers.main import Home


class Home(Home):
    @http.route()
    def web_login(self, redirect=None, *args, **kw):
        ensure_db()
        response = super(Home, self).web_login(redirect=redirect, *args, **kw)

        if not redirect and http.request.params["login_success"]:
            if (
                http.request.env["res.users"]
                .browse(http.request.uid)
                .has_group("base.group_user")
            ):
                redirect = "/web?"
            elif (
                http.request.env["res.users"]
                .browse(http.request.uid)
                .has_group("base.group_portal")
            ):
                redirect = "/"
            return http.request.redirect(redirect)
        return response


class LibraryWebPortal(http.Controller):

    @http.route('/', website=True, auth='public')
    def index(self, **kw):
        user = http.request.env["res.users"].search([('id', '=', http.request.env.context.get('uid'))])
        books_in_hand = http.request.env['library.book'].search_count([
            ('customer_id', '=', user.partner_id.id),
            ('state', '=', 'On Hand')
        ])
        books_in_library = http.request.env['library.book'].search_count([
            ('customer_id', '=', False),
            ('quantity', '>', 0)
        ])

        return http.request.render('42loc_library.index_page', {
            'books_in_hand': books_in_hand,
            'books_in_library': books_in_library
        })

    @http.route('/my_books', website=True, auth='public')
    def my_books(self, **kw):
        user = http.request.env["res.users"].search([('id', '=', http.request.env.context.get('uid'))])
        books_in_hand = http.request.env['library.book'].search([
            ('customer_id', '=', user.partner_id.id),
            ('state', '=', 'On Hand')
        ])
        return http.request.render('42loc_library.my_books', {
            'data': books_in_hand
        })

    @http.route('/library_books', website=True, auth='public')
    def library_books(self, **kw):
        books_in_library = http.request.env['library.book'].search([
            ('customer_id', '=', False),
            ('quantity', '>', 0)
        ])

        return http.request.render('42loc_library.library_books', {
            'data': books_in_library
        })

    @http.route('/book/borrow/<int:book_id>', website=True, auth='public')
    def book_borrow(self, **kw):
        return http.request.render('42loc_library.book_borrow', {
            'id': kw.get('book_id')
        })

    @http.route('/book/borrow/new', website=True, auth='public', csrf=False)
    def book_borrow_new(self, **kw):
        book = http.request.env['library.book'].search([
            ('id', '=', kw.get('book_id'))
        ])
        book.sudo().write({
            'quantity': book.quantity - 1
        })
        stage = http.request.env['library.book.stage'].search([
            ('name', '=', 'Request')
        ], limit=1)
        user = http.request.env["res.users"].search([('id', '=', http.request.env.context.get('uid'))])
        request_book = book.sudo().copy()
        request_book.sudo().write({
            'book_stage_id': stage.id,
            'customer_id': user.partner_id.id,
            'date_start': kw.get('date_start'),
            'date_end': kw.get('date_end')
        })
        return http.request.redirect('/')
