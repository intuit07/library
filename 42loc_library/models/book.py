# -*- coding: utf-8 -*-

from odoo import models, fields, api
from operator import itemgetter


class Book(models.Model):
    _name = "library.book"
    _description = "Our Books"

    def _default_stage(self):
        stage = self.env["library.book.stage"].search([("name", "=", "On Shelf")])
        return stage

    @api.model
    def _get_languages(self):
        langs = self.env["res.lang"].sudo().search([("active", "in", [True, False])])
        return sorted([(lang.code, lang.name) for lang in langs], key=itemgetter(1))

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        return self.env["library.book.stage"].search([])

    name = fields.Char(string="Book title", required=True)
    authors = fields.Char(string="Author")
    publisher_information = fields.Char(string="Author")
    category = fields.Char(string="Category")
    state = fields.Char(
        string="Status",
        store=True,
        copy=False,
        compute="_compute_state",
        default="On Shelf",
    )

    image_1920 = fields.Image()

    date_start = fields.Date(string="Book issued date")
    date_end = fields.Date(string="Expected return date")

    language = fields.Selection(_get_languages, string="Language")

    number_of_page = fields.Integer(string="Number of page")
    average_rating = fields.Integer(string="Average rating")
    quantity = fields.Integer(string="Quantity", required=True, default=1)
    quantity_on_shelf = fields.Integer(string="Quantity on shelf", compute="_compute_quantity_on_shelf")

    book_stage_id = fields.Many2one("library.book.stage", string="Book stages",
                                    group_expand="_read_group_stage_ids",
                                    default=_default_stage)
    customer_id = fields.Many2one("res.partner", string="Customer", readonly=True)
    calendar_event_ids = fields.One2many("calendar.event", "book_id")

    def _compute_quantity_on_shelf(self):
        stage_id = self.env["library.book.stage"].search([('name', '=', 'On Shelf')], limit=1).id
        self.quantity_on_shelf = self.search_count([('id', '=', self.id), ('book_stage_id', '=', stage_id)])

    @api.depends("book_stage_id")
    def _compute_state(self):
        self.state = self.book_stage_id.name

    def confirm_request(self):
        stage_id = self.env["library.book.stage"].search([('name', '=', 'On Hand')], limit=1).id
        self.book_stage_id = stage_id
        return self.env["calendar.event"].sudo().create({
            'name': self.name,
            'allday': True,
            'start_date': self.date_start,
            'stop_date': self.date_end,
            'partner_ids': [(6, 0, [self.customer_id.id])],
            'user_id': self._context.get('uid'),
            'book_id': self.id
        })

    def return_book(self):
        book_id = self.env["library.book"].search([('name', '=', self.name), ('state', '=', 'On Shelf')], limit=1)
        book_id.quantity += 1
        self.env["calendar.event"].search([('book_id', '=', self.id)]).unlink()
        self.unlink()


class BookStage(models.Model):
    _name = "library.book.stage"
    _description = "Stages for Books"

    name = fields.Char(strin="Stage name")

    book_stage_ids = fields.One2many("library.book", "book_stage_id")
