# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CalendarEventInherit(models.Model):
    _inherit = 'calendar.event'

    book_id = fields.Many2one("library.book")
