# -*- coding: utf-8 -*-

from odoo import models, fields, api
from operator import itemgetter


class LibraryProductInherit(models.Model):
    _inherit = "product.template"

    @api.model
    def _get_languages(self):
        langs = self.env["res.lang"].sudo().search([("active", "in", [True, False])])
        return sorted([(lang.code, lang.name) for lang in langs], key=itemgetter(1))

    authors = fields.Char(string="Author")
    publisher_information = fields.Char(string="Publisher information")
    category = fields.Char(string="Category")

    language = fields.Selection(_get_languages, string="Language")

    number_of_page = fields.Integer(string="Number of page")
    average_rating = fields.Integer(string="Average rating")
