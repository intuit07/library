# -*- coding: utf-8 -*-

from odoo import models, fields, api
from operator import itemgetter


class LibraryStockInherit(models.Model):
    _inherit = "stock.picking"

    date_start = fields.Date(string="Book issued date")
    date_end = fields.Date(string="Expected return date")

    calendar_event_ids = fields.One2many("calendar.event", "request_id")
    library_request_id = fields.Many2one("library.library")

    book_return = fields.Boolean(default=False)

    def button_validate(self):
        if self.picking_type_code == "outgoing":
            self.env["calendar.event"].sudo().create({
                'name': self.name,
                'allday': True,
                'start_date': self.date_start,
                'stop_date': self.date_end,
                'partner_ids': [(6, 0, [self.partner_id.id])],
                'user_id': self._context.get('uid'),
                'request_id': self.id
            })
            stage_id = self.env["library.stage"].search([('name', '=', 'My Book')], limit=1)
            self.env["library.library"].search([('id', '=', self.library_request_id.id)]).sudo().write({
                'stage_id': stage_id.id
            })
        if self.picking_type_code == "incoming":
            self.env["calendar.event"].search([('request_id', '=', self.id)]).sudo().unlink()
            self.env["library.library"].search([('id', '=', self.library_request_id.id)]).sudo().unlink()
        return super(LibraryStockInherit, self).button_validate()


class ReturnPickingInherit(models.TransientModel):
    _inherit = 'stock.return.picking'

    def _create_returns(self):
        calendar_event = self.env["calendar.event"].search([('request_id', '=', self.picking_id.id)])
        self.picking_id.book_return = True
        res = super(ReturnPickingInherit, self)._create_returns()
        record = self.env["stock.picking"].search([('id', '=', res[0])])
        record.sudo().write({
            'name': calendar_event.name + '_' + str(calendar_event.id),
            'library_request_id': self.picking_id.library_request_id.id,
        })
        calendar_event.request_id = res[0]
        return res
