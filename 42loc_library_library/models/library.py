# -*- coding: utf-8 -*-

from odoo import models, fields, api


class LibraryLibrary(models.Model):
    _name = "library.library"
    _description = "Library"

    def _default_stage(self):
        stage = self.env["library.stage"].search([("name", "=", "Request")])
        return stage

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        return self.env["library.stage"].search([])

    @api.model
    def default_get(self, fields):
        res = super(LibraryLibrary, self).default_get(fields)
        partner_id = self.env["res.users"].search([('id', '=', self._context.get('uid'))]).partner_id.id
        res.update(customer_id=partner_id)
        return res

    date_start = fields.Date(string="Book issued date", required=True)
    date_end = fields.Date(string="Expected return date", required=True)

    state = fields.Char(
        string="Status",
        store=True,
        copy=False,
        compute="_compute_state",
        default="On Shelf",
    )

    customer_id = fields.Many2one("res.partner", string="Customer")
    book_id = fields.Many2one("product.product", string="Book", required=True)
    stage_id = fields.Many2one("library.stage", string="Library stages",
                               group_expand="_read_group_stage_ids",
                               default=_default_stage)
    stock_picking_ids = fields.Many2one("stock.picking", "library_request_id")

    @api.depends("stage_id")
    def _compute_state(self):
        self.state = self.stage_id.name

    @api.model
    def create(self, vals):
        res = super(LibraryLibrary, self).create(vals)
        stock_dest = self.env['stock.location'].search([('usage', '!=', 'view')], limit=1)
        stock_location = self.env['stock.location'].search([('name', '=', 'Stock')], limit=1)
        picking_type_id = self.env['stock.picking.type'].search([('name', '=', 'Delivery Orders')], limit=1)
        self.env["stock.picking"].sudo().create({
            'name': res.book_id.name + '_' + str(res.id),
            'date_start': res.date_start,
            'date_end': res.date_end,
            'partner_id': res.customer_id.id,
            'location_id': stock_location.id,
            'location_dest_id': stock_dest.id,
            'picking_type_id': picking_type_id.id,
            'library_request_id': res.id,
            'immediate_transfer': True,
            'move_ids_without_package': [(0, 0, {
                'name': res.book_id.name,
                'product_id': res.book_id.id,
                'product_uom': res.book_id.uom_id,
                'location_id': stock_location.id,
                'location_dest_id': stock_dest.id,
                'quantity_done': 1
            })],
        })
        return res

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        new_domain = domain
        if not self.env['res.users'].search([('id', '=', self._context.get('uid'))]). \
                has_group('stock.group_stock_manager'):
            new_domain.append(['create_uid', '=', self._context.get('uid')])
        res = super(LibraryLibrary, self).search_read(new_domain, fields, offset, limit, order)
        return res


class LibraryStage(models.Model):
    _name = "library.stage"
    _description = "Stages for Library"

    name = fields.Char(strin="Stage name")

    stage_ids = fields.One2many("library.library", "stage_id")
